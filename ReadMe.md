***

![logo](/assets/image/logo.png)

***

<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=5 orderedList=false} -->

<!-- code_chunk_output -->

- [1. 凌網全球科技-前端框架](#1-凌網全球科技-前端框架)
  - [1.1. 簡介](#11-簡介)
  - [1.2. 版本控管](#12-版本控管)
  - [1.3. 開發工具](#13-開發工具)
    - [1.3.1. 外掛](#131-外掛)
  - [1.4. 專案下載位置](#14-專案下載位置)
- [2. 登入系統](#2-登入系統)
  - [2.1. 登入畫面](#21-登入畫面)
  - [2.2. 登入後的畫面](#22-登入後的畫面)
  - [2.3. 登出](#23-登出)
- [3. 功能選單](#3-功能選單)
  - [3.1. 管理者選單](#31-管理者選單)
    - [3.1.1. 路徑管理](#311-路徑管理)
      - [3.1.1.1. 查詢](#3111-查詢)
      - [3.1.1.2. 新增](#3112-新增)
      - [3.1.1.3. 修改與排序](#3113-修改與排序)
      - [3.1.1.4. 預設](#3114-預設)
    - [3.1.2. 作業方法權限管理](#312-作業方法權限管理)
    - [3.1.3. 權限管理](#313-權限管理)
    - [3.1.4. 角色管理](#314-角色管理)
      - [3.1.4.1. 查詢](#3141-查詢)
      - [3.1.4.2. 新增](#3142-新增)
      - [3.1.4.3. 更新](#3143-更新)
    - [3.1.5. 用戶](#315-用戶)
      - [3.1.5.1. 查詢用戶](#3151-查詢用戶)
      - [3.1.5.2. 新增用戶](#3152-新增用戶)

<!-- /code_chunk_output -->

---

# 1. 凌網全球科技-前端框架

## 1.1. 簡介

>本框架採用vue cli3開發，並修改[vue-element-admin](https://panjiachen.github.io/vue-element-admin-site/zh/guide/)以符合公司的需求。

## 1.2. 版本控管

>本框架採用[GIT](https://zh.wikipedia.org/wiki/Git)作為版本控管技術。

## 1.3. 開發工具

>本系統採用微軟開源ide([VSCODE](https://code.visualstudio.com/))為開發工具。

### 1.3.1. 外掛

>VSCODE所需安裝的外掛

1. [Element UI Snippets](https://marketplace.visualstudio.com/items?itemName=SS.element-ui-snippets)
2. [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
3. [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
4. [Vetur](https://marketplace.visualstudio.com/items?itemName=octref.vetur)
5. [vue-helper](https://marketplace.visualstudio.com/items?itemName=shenjiaolong.vue-helper)

## 1.4. 專案下載位置

https://gitlab.com/tw.tommy/happy-go-crm-backend.git

---

# 2. 登入系統

## 2.1. 登入畫面

>輸入帳號與密碼並按下**Login**進行登入，只要帳密錯三次就會被鎖15分鐘。

![](/assets/image/login/login.jpg "登入畫面")

## 2.2. 登入後的畫面

>登入後，左邊是功能選單，右上角的是大頭貼，中間顯示用戶ID、權限以及布告欄。

![](/assets/image/login/afterlogin.jpg "登入後的畫面")

## 2.3. 登出

>點擊右上角的**蟹老闆**將會出現**登出**，點擊**登出**後，系統將會回到登入畫面。

![](/assets/image/login/logout.jpg "登出")

---

# 3. 功能選單

## 3.1. 管理者選單

>管理者選單為權限-**ADMIN**才會顯示在畫面，建議給系統管理者此權限。裡面包括*路徑管理*、*作業方法權限管理*、*權限管理*、*角色管理*以及*用戶*。

![](/assets/image/admin/adminMenu.jpg "管理者選單")

### 3.1.1. 路徑管理

>路徑管理分別有*查詢*、*新增*、*修改與排序*、*預設*功能。

![](/assets/image/admin/path/view.jpg "路徑管理")

#### 3.1.1.1. 查詢

>查詢畫面中，查詢條件為主目錄(例:管理者、作業方法權限管理)。如不給予查詢條件，查詢結果將是全部資料。

![](/assets/image/admin/path/query.jpg "查詢")

#### 3.1.1.2. 新增

>由於前端目錄有4層，所以必須給予4層路徑。

* 選者第一層目錄將得第二層目錄選項，選擇第二層目錄將得第三層選項。以上選擇可不選，是輔助新增在現有目錄下的子目錄。**重置**按鈕將選項都清空。**帶入**按鈕將把選項的值帶入下面。

![](/assets/image/admin/path/add-step1.jpg "新增-step1")

* 按下**帶入**後下方輸入框。

![](/assets/image/admin/path/add-step2.jpg "新增-step2")

* 按下**新增**後，將會新增到資料庫。

![](/assets/image/admin/path/add-step3.jpg "新增-step3")

#### 3.1.1.3. 修改與排序

>按下查詢後得出的結果，按下**UP**或**DOWN**可進行位移，在按下**更新排序**就完成排序了。按下編輯就能進行修改或刪除。按下**復原**就會復原到原本的資料。

![](/assets/image/admin/path/update-step2.jpg "更新-step1")

![](/assets/image/admin/path/update-step3.jpg "更新-step2")

#### 3.1.1.4. 預設

>選擇好第一，第二，第三，第四層目錄後，按下**設定REDIRECT**，將會設定目錄預設的畫面。

![](/assets/image/admin/path/default.jpg "預設畫面")


### 3.1.2. 作業方法權限管理

>作業方法權限管理是為了賦予畫面特定的一些功能權限，有權限則顯示，無權限則不會顯示。選擇**路徑**後，再選擇**功能權限**(可複選)，按下**新增**就可為畫面給予權限控管。

![](/assets/image/admin/func/view.jpg "作業方法權限管理")

### 3.1.3. 權限管理

>選擇**角色**後再選擇**主標題**，按下**查詢權限**後方可選擇**階層**內的路徑，再按下**賦予權限**，角色就擁有路徑的權限。

![](/assets/image/admin/permission/view.jpg "作業方法權限管理")

### 3.1.4. 角色管理

>此功能為新增角色或更改角色的狀態。

#### 3.1.4.1. 查詢

>可查詢角色的狀態與列印報表

![](/assets/image/admin/role/query.jpg "角色查詢")

#### 3.1.4.2. 新增

>**角色敘述**為可選欄位，其他欄位為必填，按下**新增**後就可新增到資料庫。

![](/assets/image/admin/role/add.jpg "角色新增")

#### 3.1.4.3. 更新

>測試TAB用

### 3.1.5. 用戶

>此功能為查詢用戶和新增用戶。

#### 3.1.5.1. 查詢用戶

![](/assets/image/admin/user/query.jpg "查詢用戶")

#### 3.1.5.2. 新增用戶

![](/assets/image/admin/user/insert.jpg "新增用戶")