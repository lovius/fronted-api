# 凌網全球科技 前端框架

## 簡介

>本框架採用vue cli3開發，並修改[vue-element-admin](https://panjiachen.github.io/vue-element-admin-site/zh/guide/)以符合公司的需求。

## 版本控管

>本框架採用[GIT](https://zh.wikipedia.org/wiki/Git)作為版本控管技術。

## 開發工具

>本系統採用微軟開源ide([VSCODE](https://code.visualstudio.com/))為開發工具。

### 外掛
